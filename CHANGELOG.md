# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## v0.1.0
- Add support for scopes instead of definition
- Add assignment support
- imports/exports
- function definitions

## v0.0.2
- Named argument support

## v0.0.1

### Added

- AST parsing
- Added Element : FunctionCall/ASTFunctionCall, Identifier(String), Args, Value, Object, List, Int, Long, Float, Double, 
  Bool, String, None
- Value: primitive types (most elements, not identifier or args)
  